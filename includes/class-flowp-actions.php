<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( !class_exists( 'Flowp_Functions' ) ) :

	class Flowp_Functions {

		public function __construct() {

			add_action( 'template_redirect', array( $this, 'validate_and_route_request' ), 0 );

        }

        public function validate_and_route_request() {

            if( empty( $_GET['flowp_call'] ) || ($_SERVER['REQUEST_METHOD'] != 'POST') ) return;

            //params are json
            $data = json_decode( file_get_contents('php://input'), true );

            // check key
            if( !$this->validate_key( $data['integration'] ) ) return;

            $action = $data['action'];

            switch( $action ) {
                case 'wp_create_user': 
                    $this->create_user( $data );
                case 'wp_send_email': 
                    $this->send_email( $data );
                case 'wp_save_meta': 
                    $this->save_meta( $data );
            }

        }
          
        private function create_user( $req ) {
				
            $user_id = username_exists( $req['username'] );
            if ( !$user_id and email_exists( $req['email'] ) == false ) {

                // create user
                $user_id = wp_create_user( $req['username'], $req['password'], $req['email'] );

                // returns the WP_Error instance in case of error
                if( is_wp_error( $user_id ) ) {
                    echo  json_encode( (object) ["result" => "error", "message" => $user_id->get_error_message()] );
                    die();
                }

                $blocked_roles = ['administrator', 'editor', 'shop_manager'];
                $role = in_array( $req['role'], $blocked_roles ) ? 'subscriber' : $req['role'];

                // update user data and set role
                $user_data = array(
                    'ID'           => $user_id,
                    'first_name'   => $req['meta']['first_name'],
                    'last_name'    => $req['meta']['last_name'],
                    'role'         => $role
                );

                if( !empty( $req['meta']['first_name'] ) && !empty( $req['meta']['last_name'] ) ) {
                    $user_data['display_name'] = ucfirst( $req['meta']['first_name'] ) . " " . ucfirst( $req['meta']['last_name'] );
                }

                wp_update_user( $user_data );

                // Update metadata
                foreach ( $req['meta'] as $meta_key => $value ) {
                    
                    // allow 3rd party filter of meta values if custom logic is needed
                    update_user_meta( $user_id, $meta_key, apply_filters( 'flowp_filter_meta_value', $value, $meta_key ) );
                }

                if( !empty( $req['notify'] ) ) {
                    wp_new_user_notification($user_id, null, 'user');
                }

                do_action( 'flowp_wp_user_created', $user_id );

                echo  json_encode( array( 'result' => 'success', 'user_id' => $user_id ) );

            } else {
                echo  json_encode( (object) ["result" => "error", "message" => "This email is already being used."] );
            }
            
            die();

        }
        
        public function save_meta( $req ) {

            if ( !empty( $req['metaData'] ) && !empty( $req['userId'] ) ) {
                
                $user = get_userdata( $req['userId'] );
                if ( $user === false ) {
                    echo  json_encode( (object) ["result" => "error", "message" => "User does not exist"] );
                    die();
                }

                foreach ( $req['metaData'] as $meta_key => $value ) {
                    
                    // allow 3rd party filter of meta values if custom logic is needed
                    update_user_meta( $req['userId'], $meta_key, apply_filters( 'flowp_filter_meta_value', $value, $meta_key ) );
                }
                
                echo  json_encode( array( 'result' => 'success', 'user_id' => $req['userId'] ) );
                die();
            }

            echo  json_encode( (object) ["result" => "error", "message" => "Invalid Request"] );
            die();

        }

        public function send_email( $req ) {
            if ( !empty( $req['email'] ) && !empty( $req['subject'] ) && !empty( $req['message'] ) ) {
                if( wp_mail( $req['email'], $req['subject'], $req['message'], array('Content-Type: text/html; charset=UTF-8') ) ) {
                    echo  json_encode( (object) ["result" => "error"] );
                } else {
                    echo  json_encode( (object) ["result" => "sent"] );
                }
            }

            die();

        }

        // Call WordPress action
        public function call_action() {
            
        }

        private function validate_key( $key ) {
            global $flowp;

            return ( !empty($key) && ($flowp->get_integration_key() == $key) );
        }
    
    }

    new Flowp_Functions();

endif;
