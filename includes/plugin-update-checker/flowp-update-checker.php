<?php

include( __DIR__ . '/plugin-update-checker.php' );
$update_checker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/flowpapp/flowp-wordpress-plugin',
    WP_PLUGIN_DIR . '/flowp-for-wordpress/flowp.php',
    'flowp-for-wordpress'
);

// var_dump( $update_checker );
// exit;

$update_checker->setBranch('master');