<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/* 
@TODO Use the authentication header and the customer key to manage their access.
*/

if ( !class_exists( 'Flowp_Settings_Page' ) ) :

	class Flowp_Settings_Page  {

		/**
		 * @var Singleton The reference the *Singleton* instance of this class
		 */
		private static $instance;

		/**
		 * Returns the *Singleton* instance of this class.
		 *
		 * @return Singleton The *Singleton* instance.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * Private clone method to prevent cloning of the instance of the
		 * *Singleton* instance.
		 *
		 * @return void
		 */
		private function __clone() {}

		/**
		 * Private unserialize method to prevent unserializing of the *Singleton*
		 * instance.
		 *
		 * @return void
		 */
		private function __wakeup() {}

		/**
		 * Notices (array)
		 * @var array
		 */
		public $notices = array();

		/**
		 * Protected constructor to prevent creating a new instance of the
		 * *Singleton* via the `new` operator from outside of this class.
		 */
		protected function __construct() {

            add_action('admin_menu', array( $this, 'create_menu_option' ) );

        }
        
        public function create_menu_option() {
            //create new top-level menu
            // add_menu_page('Flowp Settings', 'Flowp', 'manage_options', 'flowp-settings', array( $this, 'render_settings_page' ) , 'dashicons-chart-pie' );
            
            // This page will be under "Settings"
            add_options_page(
                'Flowp Settings', 
                'Flowp', 
                'manage_options', 
                'flowp-settings', 
                array( $this, 'render_settings_page' )
            );

            //call register settings function
            add_action( 'admin_init', array( $this, 'register_flowp_settings' ) );
        }

        public function render_settings_page() {

            // Set class property
            $this->options = get_option( 'flowp-settings' );

            $this->is_connected = false;

            ob_start();
            ?>
            <div class="wrap">
                <h1>Flowp Settings</h1>


                <form method="post" action="options.php">
                    <?php settings_fields( 'flowp-settings' ); ?>
                    <?php do_settings_sections( 'flowp-settings' ); ?>
                    <?php submit_button("Connect"); ?>
                </form>

                </div>
            <?php
            echo ob_get_clean();
        }

        public function register_flowp_settings() {
            register_setting( 
                'flowp-settings', 
                'flowp-settings', 
                array( $this, 'validate' ) // Sanitize
            );

            add_settings_section(
                'flowp_settings', // ID
                null, // Title
                array( $this, 'show_validation_message' ), // Callback
                'flowp-settings' // Page
            );  

            add_settings_field(
                'integration_key', // ID
                'Integration Key', // Title 
                array( $this, 'integration_key_callback' ), // Callback
                'flowp-settings', // Page
                'flowp_settings' // Section           
            );      

        }
        
        /** 
         * Print the Section text
         */
        public function show_validation_message() {
            
            // $remote = wp_remote_get( FLOWP_URL . "/validate_key=" . $this->options['integration_key'] );
            // $remote = wp_remote_get( "https://httpbin.org/get" );
            
            // $result = $remote['body'];

            // var_dump( json_decode( $result ) );

            // print '<h3>Your website is connected to Flowp</h3>';
        }

        /** 
         * Get the settings option array and print one of its values
         */
        public function integration_key_callback() {
            printf(
                '<input type="text" id="integration_key" class="regular-text" name="flowp-settings[integration_key]" value="%s" />',
                isset( $this->options['integration_key'] ) ? esc_attr( $this->options['integration_key']) : ''
            );
        }

        /**
         * Sanitize each setting field as needed
         *
         * @param array $input Contains all settings fields as array keys
         */
        public function validate( $input ) {
            $new_input = array();

            if( isset( $input['integration_key'] ) )
                $new_input['integration_key'] = esc_attr( $input['integration_key'] );


            $key = esc_attr( $input['integration_key'] );

            $validation = $this->connect_integration_key( $key );

            if( $validation['result'] ) {
                add_settings_error('flowp_integration_notice', 'flowp_integration_notice', 'This WordPress installation has been added to your Flowp dashboard', 'updated');
            } else {
                add_settings_error('flowp_integration_notice', 'flowp_integration_notice', $validation['message'], 'error');
            }

            
            return $new_input;
        }

        private function connect_integration_key( $key ) {

            $body = [
                'key'     => $key,
                'domain'  => home_url(),
                'type'    => 'wordpress',
            ];

            $options = [
                'body' => $body
            ];
            
            $remote = wp_remote_post( FLOWP_URL . "/integrations", $options );
            
            $response = json_decode( $remote['body'] );

            $result = $response;
            $message = $response ? 'This website is connected' : 'There was an error to connect to Flowp. Check if this site is not alreay connected.';


            return [
                'result'  => $result, 
                'message' => $message
            ];

        }

	}

	Flowp_Settings_Page::get_instance();

endif;
