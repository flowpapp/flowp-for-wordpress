<?php
/*
 * Plugin Name: Flowp
 * Description: Connect your WordPress website to Flowp.
 * Version: 1.0
 * Author: Flowp
 * Author URI: https://flowp.app/
 * Requires at least: 4.7
 * Tested up to: 5.4.2
*/


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( !class_exists( 'Flowp_WordPress' ) ) :

	define( 'FLOWP_INCLUDES', plugin_dir_path(__FILE__) . 'includes' );

	define( 'FLOWP_URL', 'https://flowp.app/api' );

	require_once( FLOWP_INCLUDES . '/class-flowp-actions.php' );
	
	class Flowp_WordPress {

		/**
		 * @var Singleton The reference the *Singleton* instance of this class
		 */
		private static $instance;

		private static $version = '1';

		/**
		 * Returns the *Singleton* instance of this class.
		 *
		 * @return Singleton The *Singleton* instance.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * Private clone method to prevent cloning of the instance of the
		 * *Singleton* instance.
		 *
		 * @return void
		 */
		private function __clone() {}

		/**
		 * Private unserialize method to prevent unserializing of the *Singleton*
		 * instance.
		 *
		 * @return void
		 */
		private function __wakeup() {}

		/**
		 * Notices (array)
		 * @var array
		 */
		public $notices = array();

		/**
		 * Protected constructor to prevent creating a new instance of the
		 * *Singleton* via the `new` operator from outside of this class.
		 */
		protected function __construct() {

			add_action( 'plugins_loaded', array( $this, 'init' ) );

			include( FLOWP_INCLUDES . '/admin/class-flowp_settings_page.php' );

			include( FLOWP_INCLUDES . '/plugin-update-checker/flowp-update-checker.php' );
			
		}

		/**
		 * Init the plugin after plugins_loaded so environment variables are set.
		 */
		public function init() {

			add_shortcode( 'flowp-flow', array( $this, 'shortcode' ) );

		}

		public function version() {
			return self::$version;
		}

		/**
		 * Allow this class and other classes to add slug keyed notices (to avoid duplication)
		 */
		public function add_admin_notice( $slug, $class, $message ) {
			$this->notices[ $slug ] = array(
				'class'   => $class,
				'message' => $message,
			);
		}

		public function get_integration_key() {
			$options = get_option('flowp-settings');
			return isset($options['integration_key']) ? $options['integration_key'] : null; 
		}

		public function shortcode( $atts ) {

			if( empty( $atts['id'] ) || is_admin() ) return;

			ob_start();
			?>
			<div class="flowp-container"><iframe class="flowp-iframe" width="100%" frameBorder=0 src="https://go.flowp.app/<?php echo $atts['id'] ?>" onclick=""></iframe></div><style>.flowp-container {position: relative; overflow: hidden; padding-top: 56.25%; } .flowp-iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 0; }</style>
			<script>window.addEventListener('message', function(evt) {
			if(evt.data.id === 'flowpStepChanged') {
				document.querySelector('.flowp-container').style.paddingTop = evt.data.contentSize;
			}
		}, false);</script>
			<?php
			return ob_get_clean();
		}

	}

	$GLOBALS['flowp'] = FlowP_WordPress::get_instance();

endif;
