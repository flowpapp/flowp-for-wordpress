=== Plugin Name ===
Tags: automation, sign-up, 
Requires at least: 4.7
Tested up to: 5.5
Stable tag: 1.1
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Connect your WordPress website with Flowp.
Flowp is a Powerful automation platform that creates dynamic multi-step forms, interactions and quizes that connect with WordPress and any website.

== Description ==

**Flowp** is a Powerful automation platform that creates dynamic multi-step forms, interactions and quizes that connect with WordPress and any website.

This plugin connects your WordPress website to Flowp.

* Create new users
* Update existing users
* Send emails

For instructions, visit <a target="_blank" href="https://flowp.app/how-to-connect-flowp-and-wordpress/">the WordPress connection tutorial</a>

== Changelog ==

= 1.0.0 =
Initial Release
